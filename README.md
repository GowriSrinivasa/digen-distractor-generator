# DiGen: Distractor Generator for Multiple Choice Questions in Code Comprehension

Repository with Code/Sample Data for the paper - ***"DiGen: Distractor Generator for Multiple Choice Questions in Code Comprehension"*** - Proc. International Conference on Engineering, Technology & Education (TALE), December, 2021.
This is an automated tool that frames a Multiple Choice Question for the given code, to test the Code Comprehension ability in students. This is particularly directed towards instructors who use flipped classrooms as a mode of teaching.

The intension is to create a tool such that, when the instructor inputs the appropriate code snippet on which they want to test the students knowledge, the tool frames a Multiple Choice Questions which includes four options - the correct answer and three appropriate distractors. Hence, the tool concentrates on creating the appropriate distractors using natural language processing. 

The tool extracts the comments in the given scippet and forms appropiate key phrases with this input. These key-phrases form the basis to determine the similarity between the input code and the existing code within the database. When the appropiate distractors are selected, the input code is pushed into the database and and its problem statement can be used to form distractors for other questions. This tool is built using Python 3.6. 


---
## Features

1. Multiple stages of processing to filter and match the keywords
2. Uses a pretrained model to exract the keywords (specially trained for Computer Science terminology)
3. Capability of forming new distractors with the information available in the database
4. Results from the survey conducted mentioned in the paper


For more information, visit: *paper url*

---
	
## Usage
	
1. [Download](https://bitbucket.org/abhijitprekash959/distractor-generator/src/master/) the Latest release from BitBucket.
2. Unzip the folder 
3. Save the desired input code as a python file within the Program_test folder.  
4. Run Tag_generator.py and enter the name of the file you want to extract tags from. 
5. Tags are stored in database.test

---
	
## Installation

pandas
```bash
pip install pandas

```
numpy
```bash
pip install numpy
```

nltk
```bash
pip install nltk
```

spacy
```bash
pip install spacy
```

pprint
```bash
pip install pprint36
```

---	
## History

Version 1.0.0
	
---
## Credits
	

