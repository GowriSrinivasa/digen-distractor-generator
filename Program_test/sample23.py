#program to check if a string is a substring of another string
str1 = "abcdefgh"
str2 = "cdefag"
l1 = list(str1)
l2 = list(str2)
if(l2[0] in l1 and l2[len(l2)-1] in l1):
  idx1 = l1.index(l2[0])
  idx2 = l1.index(l2[len(l2)-1])
if(l1[idx1:idx2+1] == l2):
  print("yes")
else:
    print("no")
