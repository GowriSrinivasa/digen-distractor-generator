#Python program to check if angles of triangle are valid or not
def is_valid_triangle(a,b,c):
    if a+b+c==180 and a!=0 and b!=0 and c!=0:
        return True
    else:
        return False

# Reading Three Angles
angle_a = float(input('Enter angle a: '))
angle_b = float(input('Enter angle b: '))
angle_c = float(input('Enter angle c: '))

# Function call & making decision
if is_valid_triangle(angle_a, angle_b, angle_c):
    print('Triangle is Valid.')
else:
    print('Triangle is Invalid.')


