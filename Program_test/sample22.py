#program to check if all characters in a string are distinct
str = "abcdefghijklmno"
l = list(str)
distinct = []
check = 0
i = 0
while(check == 0 and i<len(l)):
     if l[i] not in distinct:
        distinct.append(l[i])
        check = 0
     else:
         check = 1
     i = i+1
if(check==0):
  print("yes")
else:
    print("no")

