#program to find an index at which a given integer appears in an array sorted in non-decreasing order
a = [1,2,3,4,5,6,7,8,9]
num = 5
for i in a:
    if(i == num):
      idx = a.index(i)
print(idx)
